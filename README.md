# My dotvim configuration

Execute commands below

```
#!bash

git clone https://kovalexal@bitbucket.org/kovalexal/dotvim.git ~/.vim
ln -s ~/.vim/vimrc ~/.vimrc
cd ~/.vim
git submodule init
git submodule update
```